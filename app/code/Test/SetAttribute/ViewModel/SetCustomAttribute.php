<?php

namespace Test\SetAttribute\ViewModel;

use Magento\Customer\Model\Session as Generic;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Class SetCustomAttribute
 * @package Test\SetAttribute\ViewModel
 */
class SetCustomAttribute implements ArgumentInterface
{
    /**
     * @var Generic
     */
    private $generic;

    /**
     * @var \Magento\Framework\Registry
     */
    private $_registry;

    /**
     * @var mixed
     */
    private $_product;

    public function __construct(
        Generic $generic,
        \Magento\Framework\Registry $registry
    )
    {
        $this->generic = $generic;
        $this->_registry = $registry;
        $this->_product = $registry->registry('current_product');
    }

    public function __call($method, $args)
    {
        return $this->generic->$method(...$args);
    }

    /**
     * @return mixed
     */
    public function enableOfCustomAttribute()
    {
        return $this->_product->getCustomAttribute('product_select_attribute')->getValue();
    }

    /**
     * @return bool
     */
    public function showCustomAttribute()
    {
        if ($this->enableOfCustomAttribute()) {

            return $this->_product->getCustomAttribute('product_custom_attribute')->getValue();
        }

        return false;
    }
}