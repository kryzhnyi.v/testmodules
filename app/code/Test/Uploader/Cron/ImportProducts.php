<?php

namespace Test\Uploader\Cron;

/**
 * Class ImportProducts
 * @package Test\Uploader\Cron
 */
class ImportProducts
{
    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $_dir;

    /**
     * @var \Magento\Framework\File\Csv
     */

    public function __construct(\Magento\Framework\Filesystem\DirectoryList $dir) {
        $this->_dir = $dir;
    }

    /**
     * Products Import
     */
    public function execute()
    {
        // add logging capability
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cron.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $files = scandir($this->_dir->getPath('media') . '/var/importexport/default', SCANDIR_SORT_DESCENDING);

        //last added file
        $file = $files[0];
        $fileName = $file;

        if (strpos($fileName, 'deployed') !== false) {
            $logger->info("This file has already imported");

            return;
        }

        $file = fopen($this->_dir->getPath('media') . '/var/importexport/default/' . $file, 'r');

        if (!isset($file['tmp_name'])) {

            $row = 1;
            while ($data = fgetcsv($file, 1000, ",")) {

                //skip headers
                if ($row == 1) {
                    $row++;
                    continue;
                }

                $productSku = $data[0];
                $relatedSkus = $data[1];
                $crosssellSkus = $data[2];
                $upsellSkus = $data[3];

                try {

                    $productRepository = $objectManager->create('Magento\Catalog\Model\ProductRepository');
                    $product = $productRepository->get($productSku);

                    $productLink = $objectManager->create('Magento\Catalog\Api\Data\ProductLinkInterface')
                        ->setSku($productSku)
                        ->setLinkedProductSku($relatedSkus)
                        ->setPosition(1)
                        ->setLinkType('related');
                    $links[] = $productLink;

                    $productLink = $objectManager->create('Magento\Catalog\Api\Data\ProductLinkInterface')
                        ->setSku($productSku)
                        ->setLinkedProductSku($crosssellSkus)
                        ->setPosition(2)
                        ->setLinkType('crosssell');
                    $links[] = $productLink;

                    $productLink = $objectManager->create('Magento\Catalog\Api\Data\ProductLinkInterface')
                        ->setSku($productSku)
                        ->setLinkedProductSku($upsellSkus)
                        ->setPosition(3)
                        ->setLinkType('upsell');
                    $links[] = $productLink;

                    $product->setProductLinks($links);

                    $productRepository->save($product);

                    $logger->info("successful linked related, crosssel, upsell products");

                } catch (\Exception $e) {
                    $logger->info("Cannot retrieve products from Magento: " . $e->getMessage());
                    return;
                }
            }

            fclose($file);
            rename($this->_dir->getPath('media') . '/var/importexport/default/' . $fileName, $this->_dir->getPath('media') . '/var/importexport/default/deployed_' . $fileName);
        }

        $logger->info("End of importing");
    }
}