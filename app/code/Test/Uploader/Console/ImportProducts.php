<?php

namespace Test\Uploader\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ImportProducts
 * @package Test\Uploader\Console
 */
class ImportProducts extends Command
{
    protected function configure()
    {
        $this->setName('example:sayhello');
        $this->setDescription('Demo command line');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $state = $objectManager->get('Magento\Framework\App\State');
        $state->setAreaCode('global');

        $files = scandir('pub/media/var/importexport/default', SCANDIR_SORT_DESCENDING);

        //last added file
        $file = $files[0];
        $fileName = $file;

        if (strpos($fileName, 'deployed') !== false) {
            $output->writeln("This file has already imported");

            return;
        }

        $file = fopen('pub/media/var/importexport/default/' . $file, 'r');

        if (!isset($file['tmp_name'])) {

            $row = 1;
            while ($data = fgetcsv($file, 1000, ",")) {

                //skip headers
                if ($row == 1) {
                    $row++;
                    continue;
                }

                $productSku = $data[0];
                $relatedSkus = $data[1];
                $crosssellSkus = $data[2];
                $upsellSkus = $data[3];

                try {

                    $productRepository = $objectManager->create('Magento\Catalog\Model\ProductRepository');
                    $product = $productRepository->get($productSku);

                    $productLink = $objectManager->create('Magento\Catalog\Api\Data\ProductLinkInterface')
                        ->setSku($productSku)
                        ->setLinkedProductSku($relatedSkus)
                        ->setPosition(1)
                        ->setLinkType('related');
                    $links[] = $productLink;

                    $productLink = $objectManager->create('Magento\Catalog\Api\Data\ProductLinkInterface')
                        ->setSku($productSku)
                        ->setLinkedProductSku($crosssellSkus)
                        ->setPosition(2)
                        ->setLinkType('crosssell');
                    $links[] = $productLink;

                    $productLink = $objectManager->create('Magento\Catalog\Api\Data\ProductLinkInterface')
                        ->setSku($productSku)
                        ->setLinkedProductSku($upsellSkus)
                        ->setPosition(3)
                        ->setLinkType('upsell');
                    $links[] = $productLink;

                    $product->setProductLinks($links);

                    $productRepository->save($product);

                    $output->writeln("successful");

                } catch (\Exception $e) {
                    $output->writeln("Cannot retrieve products from Magento: " . $e->getMessage());
                    return;
                }
            }


            fclose($file);
            rename('pub/media/var/importexport/default/' . $fileName, 'pub/media/var/importexport/default/deployed_' . $fileName);
        }

        $output->writeln("End of importing");
    }
}